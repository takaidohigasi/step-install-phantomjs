# install-phantomjs

Install phantomjs

# Options

- `version` This option is not required. If set, it will install by the specified version

# Changelog

## 0.1.0

- Initial version
