#!/bin/sh

phantomjs_version=$WERCKER_INSTALL_PHANTOMJS_VERSION

# old versions (to 1.9.2)
# https://code.google.com/p/phantomjs/downloads/list
# latest versions
# https://bitbucket.org/ariya/phantomjs/downloads

download_base_path="https://bitbucket.org/ariya/phantomjs/downloads"
tar_file="phantomjs-${phantomjs_version}-linux-x86_64.tar.bz2"

cd /tmp
wget "${download_base_path}/${tar_file}"
tar xfj "${tar_file}"
sudo cp "/tmp/phantomjs-${phantomjs_version}-linux-x86_64/bin/phantomjs" "/usr/local/bin"
